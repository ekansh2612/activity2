//
//  ViewController.swift
//  Activity2
//
//  Created by Chandan Swan on 2019-06-27.
//  Copyright © 2019 ekansh sharma. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController, WCSessionDelegate {
    
    @IBOutlet weak var outputLabel: UILabel!
    
    // MARK: WCSessionDelegate Functions
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String: Any], replyHandler: @escaping ([String : Any]) -> Void){
         outputLabel.text = ("\(message)")
        // output a debug message to the terminal
        print("WATCH: Got a message from watch!")
        
        // update the message with a label
       
        
        //-----------Sending acknowledgment----------//
        
        if (WCSession.default.isReachable) {
            print("PHONE: Phone found the watch")
            // construct the message you want to send
            // the message is in dictionary
            let messageToWatch = [
                "Message from Phone": "Received"
            ]
            
            
            
            // send the message to the watch
            WCSession.default.sendMessage(messageToWatch, replyHandler: nil)
            
            print("PHONE: Sent the data!")
            // self.outputLabel.text = "Message Sent to watch"
        }
        else {
            print("PHONE: Cannot find the watch")
            // self.outputLabel.text = "ERROR: Cannot find watch"
        }
        
        
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Check if the phone supports WCSession
        // Need WCSession to communicate with a watch
        if (WCSession.isSupported()) {
            print("PHONE: Yes phone supports SESSION!")
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
            print("PHONE: Phone does NOT support SESSION")
        }
    }
    
    
    @IBAction func buttonPressed(_ sender: Any) {
        // 1. output to console
        print("+HELLO! YOU PUSHED THE BUTTON!")
        // 2. output to label
        self.outputLabel.text = "Sending message to watch"
        
        // -------
        // check if the watch is paired / accessible
        if (WCSession.default.isReachable) {
            print("PHONE: Phone found the watch")
            // construct the message you want to send
            // the message is in dictionary
            let messageToWatch = [
                "Message": "Random message from phone"
            ]
            
            
            
            // send the message to the watch
            WCSession.default.sendMessage(messageToWatch, replyHandler: nil)
            
            print("PHONE: Sent the data!")
            self.outputLabel.text = "Message Sent to watch"
        }
        else {
            print("PHONE: Cannot find the watch")
            self.outputLabel.text = "ERROR: Cannot find watch"
        }
        
    }
}
