//
//  InterfaceController.swift
//  Activity2 WatchKit Extension
//
//  Created by Chandan Swan on 2019-06-27.
//  Copyright © 2019 ekansh sharma. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class InterfaceController: WKInterfaceController,WCSessionDelegate {
    
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        // Play a "click" sound when you get the message
        WKInterfaceDevice().play(.click)
        
        // output a debug message to the terminal
        print("WATCH: Got a message!")
        
        // update the message with a label
        messageLabel.setText("\(message)")
        
        
    }
    
    
    
    
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        
    }
    
    
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        if WCSession.isSupported() {
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    @IBAction func buttonPressed() {
        
        
        let applicationData = ["From Watch": "Random Message"]
        self.sendToPhone(data: applicationData)
        
        
    }
    func sendToPhone(data: [String: Any]) {
        
        if WCSession.isSupported() {
            
            let session = WCSession.default
            session.delegate = self
            session.activate()
            
            if WCSession.default.isReachable {
                
                session.sendMessage(data, replyHandler: {(_ replyMessage: [String: Any]) -> Void in
                    
                    print("ReplyHandler called = \(replyMessage)")
                    WKInterfaceDevice.current().play(WKHapticType.notification)
                },
                                    errorHandler: {(_ error: Error) -> Void in
                                        
                                        print("Error = \(error.localizedDescription)")
                })
            }
        }
    }
    
    
    @IBOutlet weak var messageLabel: WKInterfaceLabel!
    @IBOutlet weak var acknowlegmentLabel: WKInterfaceLabel!
}

